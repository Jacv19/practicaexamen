package dataBase;

import java.io.Serializable;

public class Usuario implements Serializable {

    private long _ID;
    private String nombre;
    private String usuario;
    private String contra;

    public Usuario(long _ID, String nombre, String usuario, String contra) {
        this._ID = _ID;
        this.nombre = nombre;
        this.usuario = usuario;
        this.contra = contra;
    }

    public Usuario(){
        this._ID = 0;
        this.nombre = "";
        this.usuario = "";
        this.contra = "";
    }

    public Usuario(Usuario u){
        this._ID = u.get_ID();
        this.nombre = u.getNombre();
        this.usuario = u.getUsuario();
        this.contra = u.getContra();
    }

    public long get_ID() {
        return _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContra() {
        return contra;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }
}
