package dataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class RegistroDbHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_USUARIO = "CREATE TABLE " +
            DefinirTabla.Usuario.TABLA_NAME + " (" +
            DefinirTabla.Usuario._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Usuario.NOMBRE + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Usuario.USUARIO + TEXT_TYPE + COMMA_SEP +
            DefinirTabla.Usuario.CONTRA + TEXT_TYPE +
            ") ";
    private static final String SQL_DELETE_USUARIO = "DROP TABLE IF EXISTS " +
            DefinirTabla.Usuario.TABLA_NAME;
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "sistema.db";

    public RegistroDbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_USUARIO);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //super.onDowngrade(db, oldVersion, newVersion);
        onUpgrade(db, oldVersion, newVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USUARIO);
    }



}
