package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dataBase.RegistroUsuario;
import dataBase.Usuario;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtContra;
    private Button btnIngresar;
    private Button btnRegistrarme;
    RegistroUsuario db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtContra = (EditText) findViewById(R.id.txtContra);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegistrarme = (Button) findViewById(R.id.btnResgistrarme);
        db = new RegistroUsuario(MainActivity.this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().equals("") ||
                        txtContra.getText().toString().equals("")){

                    Toast.makeText(MainActivity.this,"Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();

                }else{
                    db.openDatabase();
                    Usuario usuario = db.getUser(txtUsuario.getText().toString(),txtContra.getText().toString());
                    if(usuario == null){

                        Toast.makeText(MainActivity.this,"Los datos no existen", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(MainActivity.this,"Usuario encontrado", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, IngresoActivity.class);
                        intent.putExtra("nombre", usuario.getNombre());
                        startActivity(intent);
                    }
                    db.cerrar();
                }
            }
        });

        btnRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivity(intent);
            }
        });

    }

    public void ingresar(View v){

    }
}
