package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import dataBase.RegistroUsuario;
import dataBase.Usuario;

public class RegistroActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtContra;
    private EditText txtRContra;
    private EditText txtNombre;
    private Button btnRegistrar;
    private Button btnLimpiar;
    private Usuario savedUsuario;

    //private int savedIndex
    private int id;
    private RegistroUsuario db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        txtUsuario = (EditText) findViewById(R.id.txtUser);
        txtContra = (EditText) findViewById(R.id.txtPassword);
        txtRContra = (EditText) findViewById(R.id.txtRepPassword);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnRegistrar = (Button) findViewById(R.id.btnRegistrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        db = new RegistroUsuario(RegistroActivity.this);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().equals("") ||
                        txtContra.getText().toString().equals("") ||
                        txtRContra.getText().toString().equals("") ||
                        txtNombre.getText().toString().equals("")){

                    Toast.makeText(RegistroActivity.this,"Favor de llenar todos los campos", Toast.LENGTH_SHORT).show();

                }else if(txtContra.getText().toString().equals(txtRContra.getText().toString())){
                    //Registrar
                    Usuario nUsuario = new Usuario();

                    nUsuario.setNombre(txtNombre.getText().toString());
                    nUsuario.setUsuario(txtUsuario.getText().toString());
                    nUsuario.setContra(txtContra.getText().toString());

                    db.openDatabase();

                    if(savedUsuario == null){
                        long idx = db.insertarUsuario(nUsuario);
                        Toast.makeText(RegistroActivity.this, "Se agrego contacto con ID:" + idx, Toast.LENGTH_SHORT).show();
                        limpiar();
                        finish();
                    }
                    else{
                        db.actualizarUsuario(nUsuario, id);
                        Toast.makeText(RegistroActivity.this, "Se actualizo el registro: " + id, Toast.LENGTH_SHORT).show();
                        limpiar();
                        finish();
                    }
                    db.cerrar();
                }else{
                    Toast.makeText(RegistroActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

    }
    public void limpiar(){
        txtUsuario.setText("");
        txtContra.setText("");
        txtRContra.setText("");
        txtNombre.setText("");
    }
}
